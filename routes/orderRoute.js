const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController');
const auth = require('../auth');
const { verify } = require('jsonwebtoken');



// MVP: create a new order; Stretch: delete current cart
// functional on postman
router.post('/checkout', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        res.send(false)
    } else {
        orderController.checkout(req.body, userData.id)
            .then(resultFromController => res.send(resultFromController))
            .catch(err => res.send(err))
    }
});


// Stretch: retrieve orders
router.get('/', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    // (admin only) retrieve all orders
    // functional on postman
    if(userData.isAdmin){
        orderController.orderDetailsAdmin()
            .then(resultFromController => res.send(resultFromController))
            .catch(err => res.send(err))
        
        // (authenticated user only) retrieve all orders
        // functional on postman
        } else {
        orderController.orderDetails(userData.id)
            .then(resultFromController => res.send(resultFromController))
            .catch(err => res.send(err))
    }
});


// Stretch: retrieve an order
router.get('/:id', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    // (admin only) retrieve any order
    // functional on postman
    if(userData.isAdmin){
        orderController.orderDetailAdmin(req.params)
            .then(resultFromController => res.send(resultFromController))
            .catch(err => res.send(err))
    // (authenticated user only) retrieve order
    // functional on postman
    } else {
        orderController.orderDetail(req.params, userData.id)
            .then(resultFromController => res.send(resultFromController))
            .catch(err => res.send(err))
    }
});


// Stretch: (authenticated user only) create cart


// Stretch: (authenticated user only) edit cart



module.exports = router;