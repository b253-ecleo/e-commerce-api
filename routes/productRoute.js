const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');
const { verify } = require('jsonwebtoken');

// MVP: retrieve active products
// functional on postman and react
router.get('/', (req, res) => {
  productController
    .getActiveProducts()
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

router.get('/all', auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    productController
      .getAllProducts()
      .then((resultFromController) => res.send(resultFromController))
      .catch((err) => res.send(err));
  } else {
    res.send(false);
  }
});

// MVP: retrieve a product
// functional on postman and react
router.get('/:id', (req, res) => {
  productController
    .getProduct(req.params)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

// MVP: (admin only) create new product
// functional on postman and react
router.post('/add', auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    productController
      .addProduct(req.body)
      .then((resultFromController) => res.send(resultFromController))
      .catch((err) => res.send(err));
  } else {
    res.send(false);
  }
});

// MVP: (admin only) update/archive product
// functional on postman and react
router.patch('/:id', auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    productController
      .updateProduct(req.params, req.body)
      .then((resultFromController) => res.send(resultFromController))
      .catch((err) => res.send(err));
  } else {
    res.send(false);
  }
});

module.exports = router;
