const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');



// MVP: user registration - add new entry to user database
// functional on postman and react
router.post('/register', (req, res) => {
    userController.registerUser(req.body)
        .then(resultFromController => res.send(resultFromController))
        .catch(err => res.send(err))
});


// MVP: user authentication - check if user is already in the database
// functional on postman and react
router.post('/login', (req, res) => {
    userController.loginUser(req.body)
        .then(resultFromController => res.send(resultFromController))
        .catch(err => res.send(err))
});


// MVP: retrieve profile - retrieve user details
// functional on postman and react
router.get('/profile', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    userController.getProfile({ userId : userData.id })
        .then(resultFromController => res.send(resultFromController))
        .catch(err => res.send(err))
});


// Stretch: (admin only) set user as admin/edit user profile
// functional on postman
router.patch('/:id', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        userController.setAdmin(req.params, req.body)
            .then(resultFromController => res.send(resultFromController))
            .catch(err => res.send(err))
    } else {
        res.send(false)
    }
});



module.exports = router;