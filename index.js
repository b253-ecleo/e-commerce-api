const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoute = require('./routes/userRoute')
const productRoute = require('./routes/productRoute')
const orderRoute = require('./routes/orderRoute')

const app = express();
const port = process.env.PORT || 4000;
const db = mongoose.connection;


// DATABASE CONNECTION (NoSQL - MongoDB)
mongoose.connect('mongodb+srv://admin:admin123@batch253-ecleo.7vjvwin.mongodb.net/s42-46?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});


db.once('open', () => console.log('MongoDB Atlas connection successful.'));

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use('/users', userRoute);
app.use('/products', productRoute);
app.use('/orders', orderRoute);



// PORT CONNECTION
if(require.main === module){
    app.listen(port, () => {
        console.log(`API is now online on port ${port}`)
    })
};