const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    userId : {
        type : String,
		required : [true, 'User id is required']
    },
    products : [
        {
            productId : {
                type : String,
		        required : [true, 'Product id is required']
            },
            quantity : {
                type : Number,
		        required : [true, 'Quantity is required']
            }
        }
    ],
    totalAmount : {
        type : Number,
        required : [true]
    },
    purchasedOn : {
        type : Date,
        default : new Date()
    }
});

module.exports = mongoose.model('Order', orderSchema);