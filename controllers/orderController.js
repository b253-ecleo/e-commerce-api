const bcrypt = require('bcrypt');

const auth = require('../auth');
const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');



// router.post('/checkout',-
// functional on postman
module.exports.checkout = async (reqBody, data) => {
    let objectId = await Product.findOne({name : reqBody.name})
        .then(result => result.id)
        .catch(err => err)

    let productPrice = await Product.findOne({name : reqBody.name})
        .then(result => result.price)
        .catch(err => err)

    if(objectId){
        let newOrder = new Order({
            userId : data,
            products : [
                {
                    productId : objectId,
                    quantity : reqBody.quantity
                }
            ],
            totalAmount : productPrice * reqBody.quantity
        })
        return newOrder.save()
            .then(order => true)
    } else {
        return false
    }
};


// router.get('/',- isAdmin : true
// functional on postman
module.exports.orderDetailsAdmin = () => {
    return Order.find()
        .then(result => result)
        .catch(err => err)
};

// router.get('/',- isAdmin : false
// functional on postman
module.exports.orderDetails = (dataUserId) => {
    return Order.find({userId : dataUserId})
        .then(result => result)
        .catch(err => err)
};


// router.get('/:id',- isAdmin : true
// functional on postman
module.exports.orderDetailAdmin = (reqParams) => {
    return Order.findById(reqParams.id)
        .then(result => result)
        .catch(err => err)
};

// router.get('/:id',- isAdmin : false
// functional on postman
module.exports.orderDetail = async (reqParams, userId) => {
    let orderDetail = await Order.findById(reqParams.id)
        .then(result => result)
        .catch(err => err)

    if(orderDetail.userId == userId){
        return orderDetail
    } else {
        return 'Access denied.'
    }
};