const bcrypt = require('bcrypt');

const auth = require('../auth');
const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');



// router.post('/register',-
// functional on postman and react
module.exports.registerUser = (reqBody) => {

    return User.find({email : reqBody.email})
        .then(result => {

            if(result.length > 0) {
                return false

            } else {
                let newUser = new User({
                    firstName : reqBody.firstName,
                    lastName : reqBody.lastName,
                    email : reqBody.email,
                    password : bcrypt.hashSync(reqBody.password, 12)
                })

                return newUser.save()
                    .then(user => {
                        if(user){
                            return true
                        } else {
                            return false
                        }
                    }).catch(err => err)
            }
        }).catch(err => err)
};


// router.post('/login',-
// functional on postman and react
module.exports.loginUser = (reqBody) => {
    return User.findOne({email : reqBody.email})
        .then(result => {

            if(result == null){
                return false
            
            } else {

                const isPassword = bcrypt.compareSync(reqBody.password, result.password)

                if(isPassword){
                    return { access : auth.accessToken(result) }
                } else {
                    return false
                }
            }
        }).catch(err => err)
};


// router.post('/details',-
// functional on postman and react
module.exports.getProfile = (data) => {
    return User.findById(data.userId)
        .then(result => {
            result.password='';
            return result
        })
        .catch(err => err)
};


// router.patch('/:id,- isAdmin : true')
// functional on postman
module.exports.setAdmin = (reqParams, reqBody) => {
    return User.findByIdAndUpdate(reqParams.id, {
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        email : reqBody.email,
        isAdmin : reqBody.isAdmin
    }, {new: true})
        .then(result => {
            return `Updated successfully!
            
                    ${result}`})
        .catch(err => err)
};


// ADMIN TOKEN
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0MDk1MDAzODI0YmMzOTM0NzJhM2UwNiIsImVtYWlsIjoibGVlY2hhZXJpbkBtYWlsLmNvbSIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY3ODQyMTgwNH0.rL7GHncEsVha9UvLa0VDHc1ueKNzWsqCPAqwzQRLD3U

// CUSTOMER TOKENS
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0MDk1MDgyODI0YmMzOTM0NzJhM2UwOSIsImVtYWlsIjoiamh1bmd3aGVlaW5AbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjc4NDIxODYxfQ.WVFvyy8dH9Fht7kOmuPKE4w7SVR3mmoIgotWcWSCVp0

// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0MDk3NGJiODEyOTllM2EwYjMzNDkwYSIsImVtYWlsIjoiYWhuaHllamluQG1haWwuY29tIiwiaXNBZG1pbiI6ZmFsc2UsImlhdCI6MTY3ODQyNTYyOX0.p0DnwlVFbWbQ15B65LZpAvTsen6nkK77tLKLiUdO0YM