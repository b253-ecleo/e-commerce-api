const Product = require('../models/Product');
const auth = require('../auth');

// router.get('/',-
// functional on postman and react
module.exports.getActiveProducts = () => {
  return Product.find({ isActive: true })
    .then((result) => result)
    .catch((err) => err);
};

module.exports.getAllProducts = () => {
  return Product.find()
    .then((result) => result)
    .catch((err) => err);
};

// router.get('/:id',-
// functional on postman and react
module.exports.getProduct = (reqParams) => {
  return Product.findById(reqParams.id)
    .then((result) => result)
    .catch((err) => err);
};

// router.post('/add',-
// functional on postman and react
module.exports.addProduct = (reqBody) => {
  // To check if product is already in the database
  return Product.find({ name: reqBody.name })
    .then((result) => {
      // Product already exists
      if (result.length > 0) {
        return false;
      } else {
        // Create new product
        let newProduct = new Product({
          name: reqBody.name,
          description: reqBody.description,
          price: reqBody.price,
        });

        return newProduct
          .save()
          .then((product) => {
            if (product) {
              return true;
            } else {
              return false;
            }
          })
          .catch((err) => err);
      }
    })
    .catch((err) => err);
};

// router.patch('/:id,- isAdmin : true')
// functional on postman and react
module.exports.updateProduct = (reqParams, reqBody) => {
  return Product.findByIdAndUpdate(
    reqParams.id,
    {
      name: reqBody.name,
      description: reqBody.description,
      price: reqBody.price,
      isActive: reqBody.isActive,
    },
    { new: true }
  )
    .then((result) => {
      return true;
    })
    .catch((err) => err);
};
